package football.entities;

public abstract class AbstractEntity {
	private Integer id;

	public AbstractEntity(Integer id) {
		this.id = id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	protected static Integer checkIfNotNull(AbstractEntity entity) {
		if(entity == null) {
			throw new RuntimeException("Nieprawid�owy parametr konstruktora");
		}
		return entity.getId();
	}
}
