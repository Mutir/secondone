package football;

import football.entities.Player;
import football.entities.Team;
import football.repositories.Repository;
import football.repositories.db.PlayerRepository;
import football.repositories.db.TeamRepository;

public class Main {

	public static void main(String[] args) {
		Repository<Player> playerRepository = new PlayerRepository();
		Repository<Team> teamRepository = new TeamRepository();
		Player player = new Player(null, "JJJJJ", "JJJJJ", Utils.getDateFromText("1990-01-01", ""), 12, 2);
		playerRepository.add(player);
		playerRepository.get(player.getId());
		playerRepository.delete(player.getId());
		
		Team team = new Team(null, "Ddddd", Utils.getDateFromText("1984-04-11", ""));
		teamRepository.add(team);
		teamRepository.get(team.getId());
		teamRepository.delete(team.getId());


	}
}
